package com.tvsben.excertrak.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tvsben.excertrak.R
import com.tvsben.excertrak.model.LogEntry

class LeaderboardRecyclerAdapter (private val context: Context, private var entries: MutableList<LogEntry>) : RecyclerView.Adapter<LeaderboardRecyclerAdapter.Holder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(context).inflate(R.layout.leaderboard_list_item, parent, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return entries.count()
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindCategory(entries[position])
    }

    fun updateData(items: MutableList<LogEntry>, sortBy: String, reversed: Boolean) {
        entries = items
        when(sortBy){
            "age" -> entries.sortWith(compareBy({it.age}, {it.value}, {it.alias}))
            "value" ->  entries.sortWith(compareBy({it.value}, {it.age}, {it.alias}))
            else -> entries.sortWith(compareBy({it.alias}, {it.age}, {it.value}))
        }
        if(reversed) {
            entries.reverse()
        }

        notifyDataSetChanged()
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val userAliasLabel = itemView.findViewById<TextView>(R.id.userAliasLabel)!!
        private val valueLabel = itemView.findViewById<TextView>(R.id.valueLabel)!!
        private val userAgeLabel = itemView.findViewById<TextView>(R.id.userAgeLabel)
        fun bindCategory(entry: LogEntry) {
            userAliasLabel.text = entry.alias
            valueLabel.text = entry.value.toString()
            userAgeLabel.text = entry.age.toString()
        }
    }
}