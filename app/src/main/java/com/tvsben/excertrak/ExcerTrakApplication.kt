package com.tvsben.excertrak

import android.app.Application
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.tvsben.excertrak.model.User
import java.util.*

class ExcerTrakApplication : Application() {


    companion object {

        var user = User()
        lateinit var auth: FirebaseAuth
        lateinit var db: DatabaseReference

        fun getAge(): Int {
            val parts = user.birthdate.split('-')
            if(parts.count() == 3)
            {
                val year = parts[0].toIntOrNull()
                val month = parts[1].toIntOrNull()
                val day = parts[2].toIntOrNull()
                if(year != null && month != null && day != null)
                {
                    val dob = GregorianCalendar(year!!, month!!, day!!)
                    val today = GregorianCalendar()
                    var age = today.get(GregorianCalendar.YEAR) - dob.get(GregorianCalendar.YEAR)
                    if(today.get(GregorianCalendar.DAY_OF_YEAR) < dob.get(GregorianCalendar.DAY_OF_YEAR)) {
                        age--
                    }
                    return age
                }
            }
            return 0
        }
    }
}