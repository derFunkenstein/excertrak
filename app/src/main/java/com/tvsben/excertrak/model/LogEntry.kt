package com.tvsben.excertrak.model

class LogEntry {
    var age: Int
    var loggable: String
    var alias: String
    var value: Double

    constructor() {
        this.age = 0
        this.loggable = LoggableActivities.Weight.name
        this.alias = ""
        this.value = 0.0
    }

    constructor(age: Int, loggable: String, alias: String, value: Double) {
        this.age = age
        this.loggable = loggable
        this.alias = alias
        this.value = value
    }

    fun toMap() : Map<String, Any> {
        val result = HashMap<String, Any>()
        result["age"] = this.age
        result["loggable"] = this.loggable
        result["alias"] = this.alias
        result["value"] = this.value
        return result
    }
}