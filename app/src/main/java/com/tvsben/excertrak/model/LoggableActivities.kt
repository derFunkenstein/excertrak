package com.tvsben.excertrak.model

enum class LoggableActivities {
    Rope,
    Mile,
    Weight,
    Ladder,
    Lift
}