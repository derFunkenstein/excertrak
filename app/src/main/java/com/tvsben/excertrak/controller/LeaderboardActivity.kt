package com.tvsben.excertrak.controller

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.tvsben.excertrak.ExcerTrakApplication
import com.tvsben.excertrak.R
import com.tvsben.excertrak.adapters.LeaderboardRecyclerAdapter
import com.tvsben.excertrak.model.LogEntry
import com.tvsben.excertrak.model.LoggableActivities
import kotlinx.android.synthetic.main.activity_leaderboard.*
import kotlin.math.max

class LeaderboardActivity : AppCompatActivity() {

    lateinit var recyclerAdapter: LeaderboardRecyclerAdapter
    private var sortBy = "value"
    var items: MutableList<LogEntry> = mutableListOf()
    var minAge = 0
    var maxAge = 25

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leaderboard)
        setupAdapters()
        setupListeners()
    }

    private fun updateSorting() {
        recyclerAdapter.updateData(items, sortBy, descendingSwitch.isChecked)
    }

    private fun doNothing() {
        return
    }

    private fun getLeaders() {
        val loggable = when (spinnerLeaders.selectedItem) {
            "Barbell Lift" -> LoggableActivities.Lift
            "Jacob's Ladder" -> LoggableActivities.Ladder
            "Mile Time" -> LoggableActivities.Mile
            "Rope Pull" -> LoggableActivities.Rope
            else -> LoggableActivities.Weight
        }

        println("Loggable: ${loggable.name}")

        ExcerTrakApplication.db.child("activities")
            .orderByChild("loggable")
            .equalTo(loggable.name)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    println("Canceled: ${p0.message}")
                }

                override fun onDataChange(p0: DataSnapshot) {
                    items.clear()
                    p0.children.mapNotNullTo(items) { it.getValue<LogEntry>(LogEntry::class.java) }
                    recyclerAdapter.updateData(
                        items.asSequence()
                            .filter { it.loggable == loggable.name }
                            .filter { it.age in minAge..maxAge }
                            .toMutableList(),
                        "age",
                        false)
                }
            })
    }

    private fun setupListeners() {
        ageButton.setOnClickListener {
            sortBy = "age"
            updateSorting()
        }
        valueButton.setOnClickListener {
            sortBy = "value"
            updateSorting()
        }
        aliasButton.setOnClickListener {
            sortBy = "alias"
            updateSorting()
        }
        descendingSwitch.setOnCheckedChangeListener{ _: CompoundButton, _: Boolean ->
            updateSorting()
        }
        spinnerLeaders.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                doNothing()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                getLeaders()
            }
        }

        spinnerAgeRange.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                doNothing()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (spinnerAgeRange.selectedItem){
                    "Any age" -> {minAge = 0; maxAge = Integer.MAX_VALUE}
                    "25 and Under" -> {minAge = 0; maxAge = 25}
                    "26 to 35" -> {minAge = 26; maxAge = 35}
                    "36 to 45" -> {minAge = 36; maxAge = 45}
                    "46 to 55" -> {minAge = 46; maxAge = 55}
                    "56 and Up" -> {minAge = 56; maxAge = Integer.MAX_VALUE}
                }
                getLeaders()
            }

        }
    }

    private fun setupAdapters() {
        var activitiesAdapter = ArrayAdapter.createFromResource(applicationContext,
            R.array.activitiesArray, android.R.layout.simple_spinner_dropdown_item)
        spinnerLeaders.adapter = activitiesAdapter

        var ageAdapter = ArrayAdapter.createFromResource(applicationContext,
            R.array.ageRanges, android.R.layout.simple_spinner_dropdown_item)
        spinnerAgeRange.adapter = ageAdapter

        recyclerAdapter = LeaderboardRecyclerAdapter(applicationContext, items)
        recyclerLeaders.adapter = recyclerAdapter
        recyclerLeaders.layoutManager = LinearLayoutManager(applicationContext)
        recyclerLeaders.setHasFixedSize(true)
    }
}
