package com.tvsben.excertrak.controller

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import com.tvsben.excertrak.ExcerTrakApplication
import com.tvsben.excertrak.R
import com.tvsben.excertrak.model.LogEntry
import com.tvsben.excertrak.model.LoggableActivities
import kotlinx.android.synthetic.main.activity_logging.*

class LoggingActivity : AppCompatActivity() {

    lateinit var loggable: LoggableActivities

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logging)
        val intent = intent
        loggable = LoggableActivities.valueOf(intent.getStringExtra("Loggable"))
        layoutLoggedValue.hint = "Log your ${setHint()}"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        btnSubmit.setOnClickListener { submit() }
    }

    private fun setHint(): String {
        when (loggable){
            LoggableActivities.Ladder -> return "Jacob's Ladder feet"
            LoggableActivities.Mile -> return "mile time (6 laps, minutes as decimals)"
            LoggableActivities.Weight -> return "current weight"
            LoggableActivities.Rope -> return "Rope feet"
            LoggableActivities.Lift -> return "barbell lift weight"
        }
        return ""
    }

    private fun submit() {
        val entry = editLoggedValue.text.toString().toDoubleOrNull()
        if(entry == null || entry <= 0) {
            //validation failed
            val builder = AlertDialog.Builder(this@LoggingActivity)
            builder.setTitle("Error")
            builder.setMessage("You must specify a positive, non-zero value for ${setHint()}")
            builder.setPositiveButton("OK") { _, _ -> return@setPositiveButton }
            builder.show()
        } else {
            //validation success, post to db
            val value = entry!!
            val age = ExcerTrakApplication.getAge()
            var logEntry = LogEntry(age, loggable.name, ExcerTrakApplication.user.alias, value)
            val key = ExcerTrakApplication.db.child("activities").push().key
            val logEntryValues: Map<String, Any> = logEntry.toMap()
            val childUpdates = HashMap<String, Any>()
            childUpdates["activities/$key"] = logEntryValues
            ExcerTrakApplication.db.updateChildren(childUpdates)
            finish()
        }

    }
}
