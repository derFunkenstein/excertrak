package com.tvsben.excertrak.controller

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import com.tvsben.excertrak.ExcerTrakApplication
import com.tvsben.excertrak.R
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupUser()
        btnSubmitUser.setOnClickListener {
            submitUser()
        }
    }

    override fun onBackPressed() {
        this.setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

    fun setupUser() {
        var values = ExcerTrakApplication.user.birthdate.split('-')
        if(values.count() == 3) {
            var year = values[0].toInt()
            var month = values[1].toInt()
            var day = values[2].toInt()
            dpBirthdate.updateDate(year, month, day)
        } else {
            dpBirthdate.updateDate(2000, 0, 1)
        }
        editAlias.setText(ExcerTrakApplication.user.alias)
    }

    private fun submitUser() {
        val alias = editAlias.text.toString()
        if(alias.isEmpty()) {
            val builder = AlertDialog.Builder(this@UserActivity)
            builder.setTitle("Error")
            builder.setMessage("You must specify an alias in order to save your profile")
            builder.setPositiveButton("OK") { _, _ -> return@setPositiveButton }
            builder.show()
        } else {
            if(ExcerTrakApplication.auth?.uid != null)
            {
                val dateString = "${dpBirthdate.year}-${dpBirthdate.month}-${dpBirthdate.dayOfMonth}"
                ExcerTrakApplication.user.birthdate = dateString
                ExcerTrakApplication.user.alias = alias
                ExcerTrakApplication.db.child("users").child(ExcerTrakApplication.auth.uid!!).setValue(
                    ExcerTrakApplication.user
                )
                setResult(RESULT_OK)
                finish()
            }
        }

    }
}
