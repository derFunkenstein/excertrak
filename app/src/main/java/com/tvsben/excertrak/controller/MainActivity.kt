package com.tvsben.excertrak.controller

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.tvsben.excertrak.ExcerTrakApplication
import com.tvsben.excertrak.R
import com.tvsben.excertrak.model.LoggableActivities
import com.tvsben.excertrak.model.LoggableActivities.*
import com.tvsben.excertrak.model.User
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //companion object is used to store constants
    companion object {
        private const val RC_SIGN_IN = 123
        private const val SETUP_BDAY = 321
        private const val TAG = "ExcerTrak"
    }

    //onCreate sets up this activity before it's displayed to the screen.
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ExcerTrakApplication.auth = FirebaseAuth.getInstance()
        ExcerTrakApplication.db = FirebaseDatabase.getInstance().reference
        setSupportActionBar(findViewById(R.id.main_toolbar))
        setupButtons()
    }

    override fun onResume() {
        super.onResume()
        updateUI()
    }

    //onCreateOptionsMenu is a lifecycle event triggered by setSupportActionBar that sets up the menu.
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val signOut = menu?.findItem(R.id.action_signout)
        val profile = menu?.findItem(R.id.action_profile)
        signOut?.isVisible = ExcerTrakApplication.auth.currentUser != null
        profile?.isVisible = ExcerTrakApplication.auth.currentUser != null
        return true
    }

    //onOptionsItemSelected is a listener for any menu item selection. It's built into the AppCompatActivity class
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_signout -> {
            signOut()
            true
        }
        R.id.action_profile -> {
            setupProfile()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    //onActivityResult is a lifecycle event that watches for a child Activity to return when it is destroyed
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                updateUI()
            } else {
                if (response == null) {
                    Toast.makeText(applicationContext, R.string.sign_in_cancelled, Toast.LENGTH_SHORT).show()
                    return
                }

                if (response.error?.errorCode == ErrorCodes.NO_NETWORK) {
                    Toast.makeText(applicationContext, R.string.no_internet_connection, Toast.LENGTH_SHORT).show()
                    return
                }

                Toast.makeText(applicationContext, R.string.unknown_error, Toast.LENGTH_SHORT).show()
                Log.e(TAG, getString(R.string.sign_in_error), response.error)
            }
        }

        else if (requestCode == SETUP_BDAY) {
            updateUI()
        }
    }

    //updateUI is used when the app starts and by the startSignIn function to set up the main screen.
    private fun updateUI() {
        progressBar.visibility = View.VISIBLE
        if (ExcerTrakApplication.auth.currentUser != null){
            getProfile()
            val displayName = ExcerTrakApplication.auth.currentUser?.displayName!!
            val email = ExcerTrakApplication.auth.currentUser?.email!!
            textSignInStatus.text = String.format(getString(R.string.signed_in_as), displayName, email)
            btnSignIn.visibility = View.GONE
            layoutActionButtons.visibility = View.VISIBLE
            var adapter = ArrayAdapter.createFromResource(applicationContext,
                R.array.activitiesArray, android.R.layout.simple_spinner_dropdown_item)
            spinnerActivities.adapter = adapter
        } else {
            textSignInStatus.text = getString(R.string.not_signed_in)
            layoutActionButtons.visibility = View.GONE
            btnSignIn.visibility = View.VISIBLE
        }
        invalidateOptionsMenu()
        progressBar.visibility = View.GONE
    }

    //connect to the database and get the profile info. If no profile info, go to Profile setup page
    private fun getProfile() {
        val userEntry = ExcerTrakApplication.db.child("users").child(ExcerTrakApplication.auth.uid!!)
        userEntry.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Toast.makeText(applicationContext, "Data retrieval error.", Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.exists()) {
                    ExcerTrakApplication.user = p0.getValue(User::class.java)!!
                    if(ExcerTrakApplication.user.birthdate.isEmpty() || ExcerTrakApplication.user.alias.isEmpty()) {
                        val builder = AlertDialog.Builder(this@MainActivity)
                        builder.setTitle("Error")
                        builder.setMessage("You must setup your profile")
                        builder.setPositiveButton("OK") { _, _ ->
                            setupProfile()
                            return@setPositiveButton
                        }
                        builder.show()
                    }
                }
            }
        })
    }

    //setupProfile opens the Profile edit page
    private fun setupProfile() {
        val intent = Intent(application, UserActivity::class.java)
        startActivityForResult(intent, SETUP_BDAY)
    }

    //signOut removes your credentials from the app on the device.
    private fun signOut() {
        ExcerTrakApplication.auth.signOut()
        ExcerTrakApplication.user = User()
        updateUI()
    }

    //startSignIn launches the activity to select your account and log into the app.
    private fun startSignIn() {
        val providers = arrayListOf(
            AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.GoogleBuilder().build())

        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setTosAndPrivacyPolicyUrls(
                    "https://example.com/terms.html",
                    "https://example.com/privacy.html")
                .setAvailableProviders(providers)
                .setLogo(R.drawable.treadmill)
                .build(),
            RC_SIGN_IN
        )
    }

    //setupButtons is another UI utility method called by onCreate. Sets button click event listeners
    //uses the LoggableActivities enum class and passes a value to the handler function.
    private fun setupButtons() {
        btnSignIn.setOnClickListener{
            startSignIn()
        }

        btnLogActivity.setOnClickListener {
            val loggable = when (spinnerActivities.selectedItem) {
                "Barbell Lift" -> Lift
                "Jacob's Ladder" -> Ladder
                "Mile Time" -> Mile
                "Rope Pull" -> Rope
                else -> Weight
            }
            doActivity(loggable)
        }

        btnLeaderboard.setOnClickListener {
            doActivity(null)
        }
    }

    //the first real bit of business logic in the app. Launches the appropriate activity for each button.
    private fun doActivity(act: LoggableActivities?) {
        Log.d(TAG, "Event clicked: $act")
        if (ExcerTrakApplication.user.birthdate.isEmpty() || ExcerTrakApplication.user.alias.isEmpty()){
            val builder = AlertDialog.Builder(this@MainActivity)
            builder.setTitle("Error")
            builder.setMessage("You must setup your profile")
            builder.setPositiveButton("OK") { _, _ ->
                setupProfile()
                return@setPositiveButton
            }
            builder.show()
        } else {
            if (act == null) {
                startActivity(Intent(this, LeaderboardActivity::class.java))
            } else {
                val intent = Intent(this, LoggingActivity::class.java)
                intent.putExtra("Loggable", act.name)
                startActivity(intent)
            }
        }
    }
}
